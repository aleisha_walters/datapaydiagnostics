/**********************************************************************************************************************************
* PROCEDURE:	spDiagnosticReport
* PURPOSE:     	Custom report
* AUTHOR:      	Anna
* CREATED:     	Sep 2019
* NOTES:
***********************************************************************************************************************************
* CHANGE SUMMARY
* Date   		    Who           	  Comments
* 20/09/2019    Anna Jin          DESDP-17047 CLONE - Ability To Trigger App Diagnostic Report Through A Web Page
* 13/07/2020    Aleisha Walters   DESDP-16011 Back Pay Period has already been paid
* 28/07/2020    Dasarsh Vadugu    DESDP-19842 Fix connection for Diagnostic Report (adding permissions for this sp)
* 25/09/2020    Dasarsh Vadugu    DESDP-19922 Update permission section
* 28/07/2021    Hayden Scott      DESDP-16247 Leave Requests linked to removed Components breaks Employee Transfer Functionality
* 24/11/2021    Bill Yang         DESDP-16256 Rem History Packages Stuck In Processing Make Rem History Screen Display Weirdly
* 09/02/2022	Aleisha Walters	  DESDP- Update structure for DataPay Diagnostics, other formatting adjustments
**********************************************************************************************************************************/
IF EXISTS
(
    SELECT *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'[dbo].[spDiagnosticReport]')
          AND type IN ( N'P', N'PC' )
)
    DROP PROCEDURE [dbo].[spDiagnosticReport]

GO

CREATE PROC [dbo].[spDiagnosticReport]
(
    @companyCode NVARCHAR(255) = NULL,
    @payRunId INT = NULL
)
AS
BEGIN

    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    SET NOCOUNT ON

    IF (@companyCode IS NULL AND @payRunId IS NULL)
    BEGIN
        DECLARE @log NVARCHAR(MAX)
        SET @log = N'Error: No variables specified'
        RAISERROR(@log, 15, 1)
    END

    ---------Declare Reports and assign ID---------
    DECLARE @ScopeRelationshipBadData		INT = 1
    DECLARE @NoHeadOfHierachySet			INT = 2
    DECLARE @InactivePGComponentParam		INT = 3
    DECLARE @TransitiveProxyAssignment		INT = 4
    DECLARE @InactivePGComponentParam2		INT = 5
    DECLARE @BackPayAlreadyPaid				INT = 6
    DECLARE @LRWithDeletedCompComponents	INT = 7
    DECLARE @RemunerationChangeQueue		INT = 8
    -----------------------------------------------

    --Begin Reports--

    IF (@companyCode IS NOT NULL) -- Reports that only require Company Code

    BEGIN

        -----------------------------------------------------------------
        -- Report 1
        -- Identify Scope Relationship Bad Data
        -----------------------------------------------------------------

        SELECT epgl.*
        INTO #copyEpgl
        FROM dbo.Company c
            INNER JOIN dbo.Employee e
                ON e.CompanyId = c.Id
            INNER JOIN dbo.EmployeePayGroupLink epgl
                ON epgl.EmployeeId = e.Id
            INNER JOIN dbo.PayGroup pg
                ON pg.Id = epgl.PayGroupId
        WHERE c.CompanyCode = @companyCode

        -- Copy and store information from scope relationship table for a specific company
        SELECT sr.*
        INTO #copySr
        FROM dbo.Company c
            INNER JOIN dbo.Employee e
                ON e.CompanyId = c.Id
            INNER JOIN dbo.Person p
                ON p.Id = e.PersonId
            INNER JOIN dbo.ScopeRelationship sr
                ON sr.ChildScopeId = p.ScopeId
            INNER JOIN dbo.PayGroup pg
                ON sr.ParentScopeId = pg.ScopeId
        WHERE c.CompanyCode = @companyCode

        -- Store informations for correct records 
        SELECT pg.Name AS PGName,
               e.EmployeeCode,
               epgl.EmployeeId,
               epgl.Id AS epglId,
               sr.Id AS srId,
               epgl.StartDate AS epglStartDate,
               epgl.FinishedDate AS epglFinishedDate,
               sr.StartDate AS srStartDate,
               sr.FinishedDate AS srFinishedDate
        INTO #temp
        FROM dbo.Company c
            INNER JOIN dbo.Employee e
                ON e.CompanyId = c.Id
            INNER JOIN dbo.EmployeePayGroupLink epgl
                ON epgl.EmployeeId = e.Id
            INNER JOIN dbo.Person p
                ON p.Id = e.PersonId
            INNER JOIN dbo.ScopeRelationship sr
                ON sr.ChildScopeId = p.ScopeId
                   AND sr.StartDate = epgl.StartDate
                   AND
                   (
                       sr.FinishedDate = epgl.FinishedDate
                       OR
                       (
                           sr.FinishedDate IS NULL
                           AND epgl.FinishedDate IS NULL
                       )
                   )
            INNER JOIN dbo.PayGroup pg
                ON sr.ParentScopeId = pg.ScopeId
                   AND pg.Id = epgl.PayGroupId
        WHERE c.CompanyCode = @companyCode

        --Delete rows that have consistent date records
        DELETE ce
        FROM #copyEpgl ce
            JOIN #temp t
                ON ce.Id = t.epglId
        DELETE cs
        FROM #copySr cs
            JOIN #temp t
                ON cs.Id = t.srId


        -- Inner Join tables for bad data
        SELECT @ScopeRelationshipBadData AS reportId,
               pg.Name AS PGName,
               e.EmployeeCode,
               ce.EmployeeId,
               ce.Id AS epglId,
               ce.StartDate AS epglStartDate,
               ce.FinishedDate AS epglFinishedDate,
               cs.Id AS srId,
               cs.StartDate AS srStartDate,
               cs.FinishedDate AS srFinishedDate
        FROM dbo.Company c
            INNER JOIN dbo.Employee e
                ON e.CompanyId = c.Id
            INNER JOIN #copyEpgl ce
                ON ce.EmployeeId = e.Id
            INNER JOIN dbo.Person p
                ON p.Id = e.PersonId
            INNER JOIN #copySr cs
                ON cs.ChildScopeId = p.ScopeId
            INNER JOIN dbo.PayGroup pg
                ON cs.ParentScopeId = pg.ScopeId
                   AND pg.Id = ce.PayGroupId
        WHERE c.CompanyCode = @companyCode
        ORDER BY e.EmployeeCode,
                 epglId

        DROP TABLE #copyEpgl
        DROP TABLE #copySr
        DROP TABLE #temp

        -----------------------------------------------------------------
        -- Report 2
        -- Check if there is no Head of Hierarchy set for the Company
        -----------------------------------------------------------------

        SELECT @NoHeadOfHierachySet AS reportId,
               c.Id,
               c.CompanyCode,
               c.Name,
               c.HeadOfHierarchyPersonId,
               p.Id AS PersonId,
               CONCAT(p.FirstName, ' ', p.LastName) AS PersonName,
               ur.Id AS UserRoleId,
               r.Id AS RoleId,
               r.Name AS RoleName
        FROM dbo.[User] u
            INNER JOIN dbo.Person p
                ON p.Id = u.PersonId
            INNER JOIN dbo.Company c
                ON c.Id = u.CompanyId
            INNER JOIN dbo.UserRole ur
                ON ur.UserId = u.Id
            INNER JOIN dbo.Role r
                ON r.Id = ur.RoleId
        WHERE r.Id IN
              (
                  SELECT r.Id FROM dbo.Role r WHERE r.Name = 'Payroll Administrator'
              )
              AND c.HeadOfHierarchyPersonId IS NULL
              AND c.FinishedDate IS NULL
              AND c.CompanyCode = @companyCode

        -----------------------------------------------------------------
        -- Report 4 
        -- Check Transitive Proxy Assignment for Company
        -----------------------------------------------------------------

        SELECT @TransitiveProxyAssignment AS reportId,
               c.CompanyCode,
               pr.ProxyForPerson AS Manager1PersonId, --Manager
               e.EmployeeCode AS Manager1EmployeeCode,
               CONCAT(p.FirstName, ' ', p.LastName) AS Manager1Name,
               pr.ProxyPerson AS Proxy1PersonId,      --Proxy
               e2.EmployeeCode AS Proxy1EmployeeCode,
               CONCAT(p2.FirstName, ' ', p2.LastName) AS Proxy1Name,
               CONVERT(DATE, pr.StartDate) AS Proxy1StartDate,
               CONVERT(DATE, pr.FinishedDate) AS Proxy1FinishedDate,
               CONCAT(p2.FirstName, ' ', p2.LastName) AS Proxy1AsManager2,
               pr2.ProxyPerson AS Proxy2PersonId,     --Proxy for the Proxy
               e3.EmployeeCode AS Proxy2EmployeeCode,
               CONCAT(p3.FirstName, ' ', p3.LastName) AS Proxy2Name,
               CONVERT(DATE, pr2.StartDate) AS Proxy2StartDate,
               CONVERT(DATE, pr2.FinishedDate) AS Proxy2FinishedDate
        FROM dbo.ProxyRelationship pr
            INNER JOIN dbo.ProxyRelationship pr2
                ON pr2.ProxyForPerson = pr.ProxyPerson
                   AND pr2.StartDate >= pr.StartDate
                   AND pr2.StartDate <= pr.FinishedDate
            INNER JOIN dbo.Person p
                ON p.Id = pr.ProxyForPerson --Manager
            INNER JOIN dbo.Person p2
                ON p2.Id = pr.ProxyPerson --Proxy
            INNER JOIN dbo.Person p3
                ON p3.Id = pr2.ProxyPerson
            INNER JOIN dbo.Employee e
                ON e.PersonId = p.Id
            INNER JOIN dbo.Employee e2
                ON e2.PersonId = p2.Id
            INNER JOIN dbo.Employee e3
                ON e3.PersonId = p3.Id
            INNER JOIN dbo.Company c
                ON c.Id = e.CompanyId
        WHERE c.CompanyCode = @companyCode
              AND pr.Deleted = 0
              AND pr2.Deleted = 0
        ORDER BY pr.StartDate,
                 pr.ProxyForPerson

        ---------------------------------------------------------------------------
        -- Report 7														DESDP-16247
        -- Check if any deleted company components are linked to any leave requests
        ---------------------------------------------------------------------------

        SELECT @LRWithDeletedCompComponents AS reportId,
			   lr.Id AS LeaveRequestID,
               lr.EmployeeId,
               c.CompanyCode,
               cc.Id AS CompanyComponentID,
               cc.Name AS CompanyComponentName
        FROM dbo.LeaveRequest lr
            INNER JOIN dbo.CompanyComponent cc
                ON lr.CompanyComponentId = cc.Id
            INNER JOIN dbo.Company c
                ON c.Id = cc.companyId
        WHERE cc.Deleted = 1
              AND lr.Deleted = 0
              AND c.CompanyCode = @companyCode

        ------------------------------------------------------------------------
        -- Report 8													 DESDP-16256
        -- Check if any RemunerationChangeQueue not deleted but still processing
        ------------------------------------------------------------------------

        SELECT @RemunerationChangeQueue AS reportId,
               rp.Id,
			   c.CompanyCode,
			   e.EmployeeCode,
			   rp.DateQueued
        FROM dbo.RemunerationChangeQueue rp
            INNER JOIN dbo.Employee e
                ON e.Id = rp.EmployeeId
            INNER JOIN dbo.Company c
                ON c.Id = e.CompanyId
        WHERE rp.Deleted = 0
              AND rp.Processing = 1
              AND c.CompanyCode = @companyCode

    END


    IF (@payRunId IS NOT NULL) -- Reports that require Pay Run ID as Input

    BEGIN

        ---------------------------------------------------------------
        -- Report 3
        -- Check Pay Runs using inactive Pay Group Component Parameter
        ---------------------------------------------------------------

        -- TimeSheetValue Table
        SELECT @InactivePGComponentParam AS reportId,
               pr.Id AS PayRunId,
               t.Id AS TimesheetId,
               tv.Id AS TimesheetValueId,
               pgcp.Id,
               pgcp.Name,
               pgcp.InActive
        FROM dbo.PayRun pr
            INNER JOIN dbo.Timesheet t
                ON t.PayRunId = pr.Id
            INNER JOIN dbo.TimesheetValue tv
                ON tv.TimesheetId = t.Id
            INNER JOIN dbo.PayGroupComponentParameter pgcp
                ON pgcp.Id = tv.PayGroupComponentParameterId
        WHERE t.PayRunId = @payRunId
              AND pgcp.InActive = 1

        -- EmployeeTimeSheetValue Table
        SELECT @InactivePGComponentParam2 AS ReportId,
               pr.Id AS PayRunId,
               t.Id AS TimesheetId,
               etv.Id AS EmployeeTimesheetValueId,
               pgcp.Id,
               pgcp.Name,
               pgcp.InActive
        FROM dbo.PayRun pr
            INNER JOIN dbo.Timesheet t
                ON t.PayRunId = pr.Id
            INNER JOIN dbo.EmployeeTimesheetValue etv
                ON etv.TimesheetId = t.Id
            INNER JOIN dbo.PayGroupComponentParameter pgcp
                ON pgcp.Id = etv.PayGroupComponentParameterId
        WHERE t.PayRunId = @payRunId
              AND pgcp.InActive = 1

        ---------------------------------------------------------------
        -- Report 6
        -- Check if back pay period has already been back paid
        ---------------------------------------------------------------

        SELECT @BackPayAlreadyPaid AS reportId,
               bp.Id AS BackPayId,
               bp.PayRunId,
               DATEADD(DAY, -1, pp2.FinishedDate) AS PayPeriodEndDate,
               pp.PayPeriodExecutedForId AS PayPeriodExecutedFor,
               bppl.OldPayPacketId AS AffectedPayPacket,
               e.EmployeeCode AS EmployeeCode,
               CONCAT(p.FirstName, ' ', p.LastName) AS Employee,
               bps.Name AS BackPayStatus
        FROM dbo.BackPay AS bp
            INNER JOIN dbo.BackPayStatus AS bps
                ON bps.Id = bp.BackPayStatusId
            INNER JOIN dbo.BackPayPacketLink AS bppl
                ON bppl.BackPayId = bp.Id
            INNER JOIN dbo.PayPacket AS pp
                ON pp.Id = bppl.OldPayPacketId
            INNER JOIN dbo.PayPeriod AS pp2
                ON pp2.Id = pp.PayPeriodExecutedForId
            INNER JOIN dbo.Employee AS e
                ON e.Id = bp.EmployeeId
            INNER JOIN dbo.Person AS p
                ON p.Id = e.PersonId
            INNER JOIN dbo.Company c
                ON c.Id = e.CompanyId
        WHERE pp.PayPeriodExecutedForId IN
              (
                  SELECT pp2.PayPeriodExecutedForId
                  FROM dbo.BackPay AS bp2
                      INNER JOIN dbo.BackPayPacketLink bppl2
                          ON bppl2.BackPayId = bp2.Id
                             AND bp2.PayRunId = @payRunId
                      INNER JOIN dbo.PayPacket pp2
                          ON pp2.Id = bppl2.OldPayPacketId
              )
              AND bp.EmployeeId IN
                  (
                      SELECT bp3.EmployeeId
                      FROM dbo.BackPay AS bp3
                      WHERE bp3.PayRunId = @payRunId
                  )
              AND bp.Deleted = 0
              AND bp.PayRunId <> @payRunId
        ORDER BY e.EmployeeCode,
                 bp.PayRunId DESC,
                 pp.PayPeriodExecutedForId DESC

    END
END

GO
GRANT EXECUTE ON [dbo].[spDiagnosticReport] TO [XmasApplicationServer]
GO