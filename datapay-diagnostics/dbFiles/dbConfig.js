const config = {
    NZ_EasiPay: {
        user: 'DiagnosticsUser',
        password: 'Di@gnostics',
        server: '172.24.8.50',
        database: 'master',
        options: {
            trustServerCertificate: true,
            trustedConnection: false,
            enableArithAbort: true
        },
        port: 1433
    },
    NZ_DataPay: {
        user: 'DiagnosticsUser',
        password: 'Di@gnostics',
        server: '172.24.8.53',
        database: 'master',
        options: {
            trustServerCertificate: true,
            trustedConnection: false,
            enableArithAbort: true
        },
        port: 1433
    },
    NZ_Enterprise: {
        user: 'DiagnosticsUser',
        password: 'Di@gnostics',
        server: '172.24.8.52',
        database: 'master',
        options: {
            trustServerCertificate: true,
            trustedConnection: false,
            enableArithAbort: true
        },
        port: 1433
    },
    AU_Easipay: {
        user: 'DiagnosticsUser',
        password: 'Di@gnostics',
        server: '10.88.43.50',
        database: 'master',
        options: {
            trustServerCertificate: true,
            trustedConnection: false,
            enableArithAbort: true
        },
        port: 1433
    },
    AU_DataPay: {
        user: 'DiagnosticsUser',
        password: 'Di@gnostics',
        server: '10.88.43.50',
        database: 'master',
        options: {
            trustServerCertificate: true,
            trustedConnection: false,
            enableArithAbort: true
        },
        port: 1433
    },
    RR: {
        user: 'DiagnosticsUser',
        password: 'Di@gnostics',
        server: 'dnzakdprestore2.dsldev.local',
        database: 'master',
        options: {
            trustServerCertificate: true,
            trustedConnection: false,
            enableArithAbort: true
        },
        port: 1433
    }   
}

module.exports = config