var configOptions = require('./dbConfig');
const sql = require('mssql');
var config;

function SelectConfig (environment){
    //select the database server for live environments
    if(environment == 'Christmas_Live'){  
        config = configOptions.NZ_EasiPay;
    } 
    else if (environment == 'Christmas_live_Enterprise') {
        config = configOptions.NZ_Enterprise;
    } 
    else if (environment == 'Christmas_Live_External'){
        config = configOptions.NZ_DataPay;
    }
    else if (environment == 'Australia_Live_External') {
        config = configOptions.AU_DataPay;
    }
    else if(environment == 'Australia_Live'){
        config = configOptions.AU_Easipay;
    }
    else{
        config = configOptions.RR;
    }
}

const RunDiagnosticReport = async(environment, companyCode, payRunId) => {
    try {
        //connect to DB
        SelectConfig(environment);
        let pool = await sql.connect(config);
        console.log("Connected to server: ", config.server);

        //Run Query
        let data = pool.request().query(`USE ${environment}; EXEC dbo.spDiagnosticReport @companyCode = N'${companyCode}', @payRunId = ${payRunId}`)
        console.log(data);
        return data;
    }
    catch(error) {
        console.log(error)
    }
}

module.exports = {
    RunDiagnosticReport
}