const   express     = require('express'),
        dbOperation = require('./dbFiles/dbOperation'),
        cors        = require('cors');

const API_PORT = process.env.PORT || 5000;
const app = express();

let client;
let session;
app.use(express.json());
app.use(express.urlencoded());
app.use(cors());

app.post('/DiagnosticReport', async(req, res) => {

    const result = await dbOperation.RunDiagnosticReport(req.body.environment, req.body.companyCode, req.body.payRunId);
    
    res.send(result.recordsets);
});

app.listen(API_PORT, () => console.log(`listening on port ${API_PORT}`));