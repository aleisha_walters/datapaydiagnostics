//Transitions for pages

export const animationOne = {
    in: {
        opacity: 1
    },
    out: {
        y: '-200vh'
     }
};

export const animationSlideUp = {
    in: {
        y: -100
    },
    out: {
       y: '95vh'
    },
    end: {
        y: 0
    }
};

export const animationThree = {
    in: {
        opacity: 1,
        x: -300
    },
    out: {
       opacity: 0,
       x: 300
    },
    
};

export const transition = {
    duration: 1
};