import './App.css';
import { Routes, Route, useLocation } from 'react-router-dom';
import Home from './Pages/Home';
import Report from './Pages/Report';
import { AnimatePresence } from 'framer-motion';
import GlobalStyle from './globalStyles';

function App() {

  let location = useLocation();

  return (
    <>
      <GlobalStyle />
      <AnimatePresence>
        <Routes location={location} key={location.pathname}>
          <Route path='/' element={<Home/>} />
          <Route path='/report' element={<Report/>} />
        </Routes>
      </AnimatePresence>
    </>
  );
}

export default App;
