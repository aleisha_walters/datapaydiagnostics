import React, {createElement, useState} from 'react';
import styled from 'styled-components';
import SearchButton from './SearchButton.js';
import NoBadData from './NoBadData';
import ReportTemplates from './ReportTemplates';
import '../Pages/Report.css';

const Button = styled.button`
  background: transparent;
  box-shadow: 0px 0px 0px transparent;
  border: 0px solid transparent;
  text-shadow: 0px 0px 0px transparent;
  `

function Form() {

  const [returnedData, setreturnedData] = useState('NoResults');
  const [search, setParams] = useState({Environment: null, CompanyCode: null, PayRunId: null});

  //Update the input values each time they are changed
  const setInput = (e) => {
    const {name, value} = e.target;
    setParams(prevState => ({
       ...prevState,
       [name]: value
    }));
  }

  const clearTables = async() => {
    //remove tables with results from previous search
  }

  //Checks inputs are valid before querying the server
  const checkData = async () => {

    setreturnedData("loading");

    clearTables();

    //these conditions need to be expanded
    if (search.Environment == null) {
        alert('Please Select an Environment');
        return;
    }
    else if (search.CompanyCode == null && search.PayRunId == null){
        alert('Please enter a Company Code or Pay Run ID');
        return;
    }
    else {
        fetchData();
    }
   }

  function getReportTemplate (report){
        var reportId = report[0].reportId;
        var template = null;

        for(var i = 0; i < ReportTemplates.length; i++) {
          if (ReportTemplates[i].reportId == reportId) {
              template = ReportTemplates[i];
              break;
          }
      }
        return template;
    }
    
    function addReportHeader (width, template){

      var reportHeader = document.createElement('thead');
      
      //Add Title
      var titleRow = document.createElement('tr');
      var reportTitle = document.createElement('th');
      reportTitle.colSpan = width;
      var titleData = document.createTextNode(template.name);
      reportTitle.appendChild(titleData);
      titleRow.appendChild(reportTitle);
      reportHeader.appendChild(titleRow);

      //Add Description
      var descRow = document.createElement('tr');
      descRow.setAttribute("className", "TableDescription");
      var reportDesc = document.createElement('td');
      reportDesc.colSpan = width;
      var descData = document.createTextNode(template.description);
      reportDesc.appendChild(descData);
      descRow.appendChild(reportDesc);
      reportHeader.appendChild(descRow);

      return reportHeader
    }


  function addTable(report) {
    
    //Find the div that matches the report ID
    var table = document.getElementById(report[0].reportId);

    //Get the Template for the relevant Report
    var template = getReportTemplate(report);
    var cols = template.columnHeaders;
    var displayCols = template.displayHeaders;
    
    //Create the Report Header
    var ReportHead = addReportHeader(cols.length, template);
    table.appendChild(ReportHead);

    //Create the table column headings
    var th = document.createElement('tr');
    th.setAttribute("id", "ColumnHeadings");

    for (var h=0; h < cols.length; h++){

      var hd = document.createElement('td');
      var headerData = document.createTextNode(displayCols[h]);

      hd.appendChild(headerData);
      th.appendChild(hd);
    }
    table.appendChild(th);     
    
    //Create table data rows
    for (var r=0; r<report.length; r++){ 

        var tr = document.createElement('tr');

        for (var c=0; c < cols.length; c++){

            var td = document.createElement('td');
            var cellData = document.createTextNode(report[r][cols[c]]);

            td.appendChild(cellData);
            tr.appendChild(td);
        }
    table.appendChild(tr);      
    }

    console.log(table);
  }
  

  function ProcessResponse(response) {

    var tables = 0;
    
    for (var i=0; i<response.length; i++){

        var report = response[i];

        //Check each report in the response for data
        if(report.length > 0){         
          tables = tables + 1;
          addTable(report)
        }
    }

    if (tables == 0) {
      setreturnedData("noBadData");
    }
  }

  //Send request to server to execute dbo.spDiagnosticReport with the current inputs
  const fetchData = async () => {

    const newData = await fetch('/DiagnosticReport', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
          environment: search.Environment,
          companyCode: search.CompanyCode,
          payRunId: search.PayRunId
      })
    })
    .then(res => res.json())
    console.log(newData);

    ProcessResponse(newData);
   }

  return  (
  <div>
    <div className='form'>           
        <label>
            Environment:
            <select name="Environment" onChange={setInput}>
                <option value=""></option>
                <option value="Christmas_Dev_External">DataPay</option>
                <option value="Christmas_Dev_UAT">EasiPay</option>
                <option value="Christmas_Dev_enterprise">Enterprise</option>
                <option value="Christmas_Dev_Australia_External">DataPay Australia</option>
                <option value="Christmas_Dev_Australia">EasiPay Australia</option>
            </select>
        </label>
        <label>
            Company Code:
            <input type='text' name='CompanyCode' onChange={setInput}/>
        </label>
        <label>
            Pay Run ID:
            <input type='text' name='PayRunId' onChange={setInput}/>
        </label>
        <Button  onClick={() => checkData()}>
          <SearchButton/>
        </Button>
      </div>
      <div id="resultsArea">
      <div>
         {returnedData == 'noBadData'? <NoBadData/> : <div/>}
      </div>
      <div id="reportTables">
        <table id="1"></table>
        <table id="2"></table>
        <table id="3"></table>
        <table id="4"></table>
        <table id="5"></table>
        <table id="6"></table>
        <table id="7"></table>
        <table id="8"></table>
      </div>
      </div>
  </div>
  );
}
export default Form;