import React from 'react';
import styled from 'styled-components';
import arrow from '../Images/downarrow.png';
import Boop from '../Animations/Boop.js';

const Button = styled.button`
  background: transparent;
  box-shadow: 0px 0px 0px transparent;
  border: 0px solid transparent;
  text-shadow: 0px 0px 0px transparent;
  `

const DownArrow = () => {
    return (
        <div>
            <Boop y={20} timing={200}>
            <Button>
              <img src={arrow} alt="GoArrow" width="70" height="50"/>
            </Button>
          </Boop>
        </div>
    )
}

export default DownArrow;
