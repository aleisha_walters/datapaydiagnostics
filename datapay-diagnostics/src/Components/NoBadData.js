import React from "react";
import robot from "../Images/bluerobot.png";

const NoBadData = () => {
    return (
        <div className='noBadData'>
            <img src={robot} alt="robot.png"/>
            <div>
                No Bad Data was Found.
            </div>
            
        </div>
    )
}

export default NoBadData
