//Name = Report Title
//Description = What it means
//ColumnHeaders = What the sql script returns
//DisplayHeaders = What to name the columns to display

const ReportTemplates = [
    {
        reportId: 1,
        name: "Bad Data between Scope Relationship and Employee Paygroup Link",
        description: "The start and finished dates in these Scope relationships are not in sync with Employee Paygroup Links",
        columnHeaders: [ "PGName", "EmployeeCode", "epglStartDate", "epglFinishedDate", "srId", "srStartDate", "srFinishedDate" ],
        displayHeaders: [ "Pay Group Name", "Employee Code", "Pay Group Link Start", "Pay Group Link Finished", "Scope Relationship Id", "Scope Relationship Start", "Scope Relationship End" ]
    },
    {
        reportId: 2,
        name: "Company has no Head of Hierarchy",
        description: "A Payroll Admin set, but no HOH set for companies, might lead to the problem 'Could not retrieve data'",
        columnHeaders: [ "Id", "CompanyCode", "Name", "HeadOfHierarchyPersonId", "PersonId", "PersonName", "UserRoleId", "RoleId", "RoleName" ],
        displayHeaders: [ "Id", "Company Code", "Name", "HeadOfHierarchyPersonId", "Person Id", "Person Name", "UserRoleId", "Role Id", "Role Name" ]
    },
    {
        reportId: 3,
        name: "Inactive Pay Group Component Parameter",
        description: "If a pay run has Timesheet Values from an inactive Pay Group Component Parameter it could cause issues viewing the online timesheet.",
        columnHeaders: ["PayRunId", "TimesheetId", "TimesheetValueId", "Id", "Name", "InActive"],
        displayHeaders: ["Pay Run Id", "Timesheet Id", "Timesheet Value Id", "Id", "Name", "Inactive"],
    },
    {
        reportId: 4,
        name: "Transitive Proxy Assignment",
        description: "Cannot end date proxy assignment where proxy person is proxy for person in another proxy assignment.",
        columnHeaders: ["CompanyCode", "Manager1PersonId", "Manager1EmployeeCode", "Manager1Name", "Proxy1PersonId", "Proxy1EmployeeCode", "Proxy1Name", "Proxy1StartDate", "Proxy1FinishedDate", "Proxy1AsManager2", "Proxy2PersonId", "Proxy2EmployeeCode", "Proxy2Name", "Proxy2StartDate", "Proxy2FinishedDate" ],
        displayHeaders: ["Company Code", "Manager1PersonId", "Manager1EmployeeCode", "Manager1Name", "Proxy1PersonId", "Proxy1EmployeeCode", "Proxy1Name", "Proxy1StartDate", "Proxy1FinishedDate", "Proxy1AsManager2", "Proxy2PersonId", "Proxy2EmployeeCode", "Proxy2Name", "Proxy2StartDate", "Proxy2FinishedDate" ]
    },
    {
        reportId: 5,
        name: "Inactive Employee Pay Group Component Parameter",
        description: "If a pay run has Employee Timesheet Values from an inactive Pay Group Component Parameter it could cause issues viewing the online timesheet.",
        columnHeaders: ["PayRunId", "TimesheetId", "EmployeeTimesheetValueId", "Id", "Name", "InActive"],
        displayHeaders: ["Pay Run Id", "Timesheet Id", "Employee Timesheet Value Id", "Id", "Name", "Inactive"]
    },
    {
        reportId: 6,
        name: "Back Pay has already been Paid",
        description: "Adding back pay for the same time period for an employee more than once may result in certain things not being picked up..",
        columnHeaders: ["BackPayId", "PayRunId", "PayPeriodEndDate", "PayPeriodExecutedFor", "AffectedPayPacket", "EmployeeCode", "Employee", "BackPayStatus"],
        displayHeaders: ["BackPay Id", "Pay Run Id", "Pay Period End Date", "Pay Period Executed For", "Affected Pay Packet", "Employee Code", "Employee", "BackPay Status"]
    },
    {
        reportId: 7,
        name: "Deleted company components are linked to Leave Requests",
        description: "If any deleted company components are linked to any leave requests this will cause issues.",
        columnHeaders: ["LeaveRequestID", "EmployeeId", "CompanyCode", "CompanyComponentID", "CompanyComponentName"],
        displayHeaders: ["Leave Request ID", "Employee Id", "Company Code", "Company Component ID", "Company Component Name"]
    },
    {
        reportId: 8,
        name: "Remuneration Change Queue Jobs are still processing",
        description: "The Following Remuneration Change Queue Jobs are still processing or may have become stuck.",
        columnHeaders: ["Id", "CompanyCode", "EmployeeCode", "DateQueued"],
        displayHeaders: ["Job ID", "Company Code", "Employee Code", "Date Queued"]
    }

]

export default ReportTemplates;