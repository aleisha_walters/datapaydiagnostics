import React from 'react';
import styled from 'styled-components';
import search from '../Images/search.png';
import Boop from '../Animations/Boop.js';

const Button = styled.button`
  background: transparent;
  box-shadow: 0px 0px 0px transparent;
  border: 0px solid transparent;
  text-shadow: 0px 0px 0px transparent;
  `

const SearchButton = () => {
    return (
        <div>
            <Boop scale={1.1} rotation={20}>
            <Button>
                <img src={search} alt="Search" width="40" height="40"/>
            </Button>
          </Boop>
        </div>
    )
}

export default SearchButton;