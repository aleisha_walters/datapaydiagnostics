import React from 'react';
import DownArrow from '../Components/DownArrow';
import logo from "../Images/logo.png"
import { Link } from 'react-router-dom';
import './Home.css';
import { animationOne, transition } from '../Animations';
import { motion } from 'framer-motion';

const Home = () => {
    return (
        <motion.div 
        initial='in' 
        animate='in' 
        exit='out'
        variants={animationOne}
        transition={transition}
        >
        <div className="container-fluid homepage-bgimage">
            <header className="App-header">
                <h3>
                    DataPay
                    <br />
                    Diagnostics
                </h3>
                <div>
                    <img src={logo} alt="Logo" width="250" height="33"/>
                </div>
                <div class="GoArrow">
                    <Link to='/Report'>
                        <DownArrow/>
                    </Link> 
                </div>
            </header>
        </div>
        </motion.div>
    );
}

export default Home
