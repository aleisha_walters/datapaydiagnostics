import React from 'react';
import './Report.css';
import { motion } from 'framer-motion';
import { animationSlideUp } from '../Animations';
import Form from '../Components/DiagnosticReport.js';

const Report = () => {
    return (
        <motion.div 
        initial='out' 
        animate='end'
        variants={animationSlideUp}
        >
            <div class="container-fluid report-bgimage">
                <div>
                    <Form/>
                </div>
                <div>
                </div>
            </div>
        </motion.div>
    )
}

export default Report
